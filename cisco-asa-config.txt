! --------------------------------------------------------------------------------
! This example configuration shows what WE did to get Amazon VPC working with our
! ASAs.  We use version 8.3(1). This config has not been reviewed or otherwise
! blessed in any way by anyone at Amazon.  YMMV.
! 
! It differs from Amazon's supplied config by using two different sets of
! crypto maps and ACLs, so it brings both tunnels up simultaneously.
!
! For the purposes of the example, the physical datacenter network is 172.16.1.0/24
! and the VPC is 10.0.0.0/16.
! --------------------------------------------------------------------------------

! --------------------------------------------------------------------------------
! You will need to allow some ICMP traffic to the outside interface for the
! "sla monitor" to work.
!
! NOTE: You should change the name of this ACL to match whatever you use on the
! outside interface of your firewall.
! --------------------------------------------------------------------------------

access-list acl_in remark ICMP for ping and traceroute
access-list acl_in extended permit icmp any any unreachable
access-list acl_in extended permit icmp any any echo
access-list acl_in extended permit icmp any any echo-reply
access-list acl_in extended permit icmp any any time-exceeded
access-list acl_in extended deny icmp any any

!
! You probably already have this ACL bound to your outside interface.  If so,
! don't duplicate it.  The command is shown below for reference only.
!
! access-group acl_in in interface outside


! --------------------------------------------------------------------------------
! Allow traffic to go between the local and VPC network without NAT processing
! --------------------------------------------------------------------------------

object network local-net
 subnet 172.16.1.0 255.255.255.0

object network vpc-0123abcd
 subnet 10.0.0.0 255.255.0.0

nat (inside,outside) source static local-net local-net destination static vpc-0123abcd vpc-0123abcd


! --------------------------------------------------------------------------------
! Recommended IPSec settings from Amazon
! --------------------------------------------------------------------------------

sysopt connection tcpmss 1387

crypto ipsec security-association lifetime seconds 3600

crypto ipsec security-association replay window-size 128
crypto ipsec df-bit clear-df outside

crypto ipsec transform-set transform-amzn esp-aes esp-sha-hmac

crypto isakmp policy 200
 authentication pre-share
 encryption aes
 hash sha
 group 2
 lifetime 28800


! --------------------------------------------------------------------------------
! Setup one cryptomap with two possible peers (A.B.C.D and E.F.G.H)
! --------------------------------------------------------------------------------

access-list acl-amzn extended permit ip any object vpc-0123abcd

crypto map vpn_client_stat_map 200 match address acl-amzn
crypto map vpn_client_stat_map 200 set pfs
crypto map vpn_client_stat_map 200 set peer A.B.C.D E.F.G.H
crypto map vpn_client_stat_map 200 set transform-set transform-amzn

tunnel-group A.B.C.D type ipsec-l2l
tunnel-group A.B.C.D ipsec-attributes
 pre-shared-key AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

tunnel-group E.F.G.H type ipsec-l2l
tunnel-group E.F.G.H ipsec-attributes
 pre-shared-key BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB

! --------------------------------------------------------------------------------
! Establish some constant traffic over the link so that it won't be torn down
!
! Note that the sla monitor won't SEEM to work, since VPC "gateways" are
! not configured to answer pings, but even the 1-way traffic will keep
! the tunnel "live" from the AWS point-of-view.
!
! The target IP should be the first possible IP in your VPC range, so for
! 10.0.0.0/16 it would be 10.0.0.1
! --------------------------------------------------------------------------------

sla monitor 200
 type echo protocol ipIcmpEcho 10.0.0.1 interface outside
 frequency 5
sla monitor schedule 1 life forever start-time now

